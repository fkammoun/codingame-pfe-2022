export default  {

    /** GET_USER_BY_LOGIN. */ GET_USER_BY_LOGIN: `http://localhost:8080/CLIENT/api/users/admin`,
    /** GET_ALL_USERS. */ GET_ALL_USERS: `http://localhost:8080/CLIENT/api/users`,

  /** Categories **/
  GET_CATEGORIES:`http://localhost:8765/evaluation/category/all`,
  GET_CATEGORY:`http://localhost:8765/evaluation/category/find/`,
  CREATE_CATEGORY:`http://localhost:8765/evaluation/category/create/`,
  UPDATE_CATEGORY:`http://localhost:8765/evaluation/category/update/`,
  DELETE_CATEGORY:`http://localhost:8765/evaluation/category/delete/`,

  /** Technologies **/
  GET_TECHNOLOGIES:`http://localhost:8765/evaluation/techno/all`,
  GET_TECHNOLOGY:`http://localhost:8765/evaluation/techno/find/`,
  CREATE_TECHNOLOGY:`http://localhost:8765/evaluation/techno/create/`,
  UPDATE_TECHNOLOGY:`http://localhost:8765/evaluation/techno/update/`,
  DELETE_technology:`http://localhost:8765/evaluation/techno/delete/`,

  /** Levels **/
  GET_LEVELS:`http://localhost:8765/evaluation/level/all`,
  GET_LEVEL:`http://localhost:8765/evaluation/level/find/`,
  CREATE_LEVEL:`http://localhost:8765/evaluation/level/create/`,
  UPDATE_LEVEL:`http://localhost:8765/evaluation/level/update/`,
  DELETE_LEVEL:`http://localhost:8765/evaluation/level/delete/`,

  /** Difficulties **/
  GET_DIFFICULTIES:`http://localhost:8765/evaluation/difficulty/all`,
  GET_DIFFICULTY:`http://localhost:8765/evaluation/difficulty/find/`,
  CREATE_DIFFICULTY:`http://localhost:8765/evaluation/difficulty/create/`,
  UPDATE_DIFFICULTY:`http://localhost:8765/evaluation/difficulty/update/`,
  DELETE_DIFFICULTY:`http://localhost:8765/evaluation/difficulty/delete/`,

  /** Deals **/
  GET_DEALS:`http://localhost:8765/evaluation/deal/all`,
  GET_DEAL:`http://localhost:8765/evaluation/deal/find/`,
  CREATE_DEAL:`http://localhost:8765/evaluation/deal/create/`,
  UPDATE_DEAL:`http://localhost:8765/evaluation/deal/update/`,
  DELETE_DEAL:`http://localhost:8765/evaluation/deal/delete/`,

  /** Access **/
  GET_ACCESSS:`http://localhost:8765/evaluation/access/all`,
  GET_ACCESS:`http://localhost:8765/evaluation/access/find/`,
  CREATE_ACCESS:`http://localhost:8765/evaluation/access/create/`,
  UPDATE_ACCESS:`http://localhost:8765/evaluation/access/update/`,
  DELETE_ACCESS:`http://localhost:8765/evaluation/access/delete/`,

  /** Question **/
  GET_QUESTIONS:`http://localhost:8765/evaluation/question/all`,
  GET_QUESTION:`http://localhost:8765/evaluation/question/find/`,
  CREATE_QUESTION:`http://localhost:8765/evaluation/question/create/`,
  UPDATE_QUESTION:`http://localhost:8765/evaluation/question/update/`,
  DELETE_QUESTION:`http://localhost:8765/evaluation/question/delete/`,


  /** Packqge **/
  GET_PACKAGES:`http://localhost:8765/evaluation/pack/all`,
  GET_PACKAGE:`http://localhost:8765/evaluation/pack/find/`,
  CREATE_PACKAGE:`http://localhost:8765/evaluation/pack/create/`,
  UPDATE_PACKAGE:`http://localhost:8765/evaluation/pack/update/`,
  DELETE_PACKAGE:`http://localhost:8765/evaluation/pack/delete/`,
  /** access **/
  GET_RESPONSES:`http://localhost:8765/evaluation/response/all`,
  GET_RESPONSE:`http://localhost:8765/evaluation/response/find/`,
  GET_RESPONSE_BY_QUESTION :`http://localhost:8765/evaluation/response/find/question/`,
  DELETE_RESPONSE_BY_QUESTION :`http://localhost:8765/evaluation/response/delete/question/`,
  CREATE_RESPONSE:`http://localhost:8765/evaluation/response/create/`,
  UPDATE_RESPONSE:`http://localhost:8765/evaluation/response/update/`,
  DELETE_RESPONSE:`http://localhost:8765/evaluation/response/delete/`,

  /** company **/
  DELETE_COMPANY:'http://localhost:8765/evaluation/company/delete/',
  GET_COMPANY:'http://localhost:8765/evaluation/company/all',
  SIGNUP_COMPANY:'http://localhost:8765/evaluation/auth/signupCompany',


  /** candidate **/
  IMPORT_File:'http://localhost:8765/evaluation/test/import-excel',
  DELETE_CANDIDATE:'http://localhost:8765/evaluation/candidate/delete/',
  GET_CANDIDATE:'http://localhost:8765/evaluation/candidate/all',
  GET_CANDIDATEByCompany:'http://localhost:8765/evaluation/candidate/findByCompany',
  SIGNUP_CANDIDATE:'http://localhost:8765/evaluation/auth/signupCandidate',
/** test **/
  GET_DONE_TEST:'http://localhost:8765/evaluation/test/find',
  EXPORT_DONE_TEST:'http://localhost:8765/evaluation/test',
  GET_INTERVAL_STAT:'http://localhost:8765/evaluation/test/statinterval',
  GET_AVG_TECH:'http://localhost:8765/evaluation/test/moyentechnology',


  GET_RESULT_DETAIL:'http://localhost:8765/evaluation/test/testresult/',

}

