
export function notifyVue(verticalAlign, horizontalAlign,invalid,valid) {
    if (this.$v.$invalid) {
      this.$notifications.notify({
        message: invalid,
        icon: "nc-icon nc-app",
        horizontalAlign: horizontalAlign,
        verticalAlign: verticalAlign,
        type: "danger"
      });
    } else {

      this.$notifications.notify({
        message: valid,
        icon: "nc-icon nc-app",
        horizontalAlign: horizontalAlign,
        verticalAlign: verticalAlign,
        type: "success"
      });
      console.log(this.user);
    }
  }
