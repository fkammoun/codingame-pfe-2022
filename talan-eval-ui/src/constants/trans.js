export const DEFAULT_LANGUAGE = 'fr'
export const FALLBACK_LANGUAGE = 'en'
export const SUPPORTED_LANGUAGES = ['fr', 'en']
