import Vue from 'vue'
import VueRouter from 'vue-router'
import LightBootstrap from './light-bootstrap-main'
//Form validation
import Vuelidate from "vuelidate";
//Store
import store from './store'
// Plugins
import App from './App.vue'
import "@/plugins/echarts";

import HighchartsVue from 'highcharts-vue'
//Traduction
import { i18n } from '@/plugins/i18n'


import { Trans } from './plugins/Translation'
import Vuex from 'vuex'
import axios from 'axios';
// import { faUserSecret } from '@fortawesome/free-solid-svg-icons';
// import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
// import store from "./store";
import vuetify from 'vuetify'
// import { library } from '@fortawesome/fontawesome-svg-core';
// import { faFontAwesome } from '@fortawesome/free-brands-svg-icons'

//library.add(faFontAwesome)

Vue.config.productionTip = false
export const bus = new Vue();

// router setup
import routes from './routes/routes'
// import '@fortawesome/fontawesome-free/css/all.css'
// import '@fortawesome/fontawesome-free/js/all.js'
// import { icon } from '@fortawesome/fontawesome-svg-core'
// import { faPlus } from '@fortawesome/free-solid-svg-icons'

Vue.use(HighchartsVue)

// library.add(faUserSecret)
// Vue.component('font-awesome-icon', FontAwesomeIcon)
// Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.use(Vuex)
Vue.prototype.$i18nRoute = Trans.i18nRoute.bind(Trans)
// plugin setup
Vue.use(Vuelidate)
// Vue.use(vuetify)

// plugin setup
Vue.use(VueRouter)
Vue.use(LightBootstrap)

//Add request interceptors
axios.interceptors.request.use((config)=>{
  let token = localStorage.getItem('TOKEN');

    if (token) {
      config.headers['Authorization'] = `Bearer ${ token }`;
    }
  console.log('Request:',config);
  return config;
},
(error)=>{
})

//Add response interceptors
axios.interceptors.response.use((response)=>{
  console.log('response:',response.data);
  return response;
},
(error)=>{

})


// configure router
const router = new VueRouter({
  routes, // short for routes: routes
  linkActiveClass: 'active'
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  render: h => h(App),
  router,
  i18n,
  store,

})

