import Vue from 'vue'
import Vuex from 'vuex'
import VueResource from 'vue-resource'
import http from "./environnement";
import VueJwtDecode from 'vue-jwt-decode'
import axios from 'axios'
import swal from "sweetalert2";
import { Store } from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import * as Cookies from 'js-cookie'

Vue.use(Vuex)
Vue.use(VueResource)
;

const state = {
  token: localStorage.getItem('TOKEN') || null,
  roles: null,
  loggedIn: false,
  path: '',
  UnusedAccess:0,
  username:'',
  question:null,
  responses:[],
  categories:[],
  levels:[],
  technologies:[],
  difficulties:[],
  candidates:[],
  pack:{},
  duration:'',
  QuestionsResponses:[],
  conditionTable:[],
  tentativeness:0,
  allQuestions:[],
  levelName:'',
  testScores:[]

}

const getters = {
  loggedIn(state) {
    return state.token !== null
  },
  token: state => state.token,


}

const mutations ={
  addCandidates(state, candidates) {
  state.candidates = candidates
},
addPack(state, pack) {
  state.pack = pack
},

  addDuration(state, duration) {
    state.duration = duration
  },

  addLevel(state, level) {
    state.levelName = level
  },
  RETREIVE_TEST_SCORES(state,testScores){
    state.testScores=testScores;
  },

  RETREIVE_TOKEN(state, TOKEN) {
    state.token = TOKEN
  },
  RETREIVE_TENTATIVENESS(state, tentativeness) {
    state.tentativeness = tentativeness
  },
  RETREIVE_USERNAME(state, username) {
    state.username = username
  },
  RETREIVE_CONDITIONTABLE(state,conditionTable){
    state.conditionTable=conditionTable
  },
  RETREIVE_ROLE(state, ROLE) {
    state.roles = ROLE
  },
  DESTROY_TOKEN(state) {
    state.token = null
  },
  DECODE_TOKEN(state, ROLES) {
    state.roles = ROLES

  },
  RETREIVE_QuestionsResponses(state,QuestionsResponses){
    state.QuestionsResponses=QuestionsResponses
  },
  RETREIVE_UNUSED_ACCESS(state,UNUSEDACCESS){
    state.UnusedAccess=UNUSEDACCESS
  },
  changeQuestion (state, payload) {
    state.question = payload
  },
  changeResponses (state, payload) {
    state.responses = payload
  },
  allQuestions(state,payload){
    state.allQuestions = payload
  },

}

const actions = {
  addCandidates(context, candidates) {
    context.commit('addCandidates',candidates)
  },
  addPack(context, pack) {
    context.commit('addPack',pack)
  },
  addDuration(context, duration) {
    context.commit('addDuration',duration)
  },
  addLevel(context, level) {
    context.commit('addLevel',level)
  },
  saveAllQuestions(context,questions){
    context.commit('allQuestions',questions)
  },

  retrieveQuestion(context,question){
    context.commit('changeQuestion',question)

  },

  decodeToken(context){
    var roles=VueJwtDecode.decode((localStorage.getItem('TOKEN'))).auth
    console.log('aaaaaaaaaaa',roles)
    context.commit('DECODE_TOKEN', roles)
  },
  retrieveToken(context, token) {
    context.commit('RETREIVE_TOKEN', token)
  },
  retrieveTentativeness(context, tentativeness) {
    context.commit('RETREIVE_TENTATIVENESS', tentativeness)
  },
  retrieveRole(context, Role) {
    context.commit('RETREIVE_ROLE', Role)
  },
  retrieveUsername(context, username) {
    context.commit('RETREIVE_USERNAME', username)
  },
  retrieveTestQuestionsResponses(context,QuestionsResponses){
    context.commit('RETREIVE_QuestionsResponses', QuestionsResponses)
  },
  retrieveConditionTable(context, conditionTable) {
    context.commit('RETREIVE_CONDITIONTABLE', conditionTable)
  },
  retrieveUnusedAccess(context, unusedaccess) {
    context.commit('RETREIVE_UNUSED_ACCESS', unusedaccess)
  },
  retrieveTestScores(context, testScores) {
    context.commit('RETREIVE_TEST_SCORES', testScores)
  },
  destroyToken(context) {
    http.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token

    if (context.getters.loggedIn) {
      http.post('/logout')
        .then(response => {
          localStorage.removeItem('TOKEN')
          context.commit('DESTROY_TOKEN')
        })
        .catch(error => {
          localStorage.removeItem('TOKEN');
          context.commit('DESTROY_TOKEN')

        })
    }
  }

}

let store = new Vuex.Store({
  plugins: [createPersistedState()],
  state: state,
  mutations: mutations,
  getters: getters,
  actions: actions,

})

export default store
