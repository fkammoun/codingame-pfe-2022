import DashboardLayout from 'src/pages/Dashboard/Layout/DashboardLayout.vue'
import DashboardBo from 'src/BO/pages/Dashboard/Layout/DashboardLayout.vue'
import DashboardExaminateur from 'src/EXAMINATEUR/pages/Dashboard/Layout/DashboardLayout.vue'
import DashboardCompany from 'src/Company/pages/Dashboard/Layout/DashboardLayout.vue'
import DashboardCandidate from 'src/CANDIDAT/pages/Dashboard/Layout/DashboardLayout.vue'

// GeneralViews
import NotFound from 'src/pages/GeneralViews/NotFoundPage.vue'
import Stats from 'src/pages/Dashboard/Dashboard/Stats.vue'
// Dashboard pages
import Overview from 'src/pages/Dashboard/Dashboard/Overview.vue'
import OverviewBo from 'src/pages/Dashboard/Dashboard/Overview.vue'

// Pages
import User from 'src/pages/Dashboard/Pages/UserProfile.vue'
import TimeLine from 'src/pages/Dashboard/Pages/TimeLinePage.vue'

import Login from 'src/AUTH/pages/Dashboard/Pages/Login.vue'
import Register from 'src/AUTH/pages/Dashboard/Pages/Register.vue'
import Lock from 'src/AUTH/pages/Dashboard/Pages/Lock.vue'
import Logout from 'src/AUTH/pages/Dashboard/Pages/Logout.vue'

// Components pages
import Buttons from 'src/pages/Dashboard/Components/Buttons.vue'
import GridSystem from 'src/pages/Dashboard/Components/GridSystem.vue'
import Panels from 'src/pages/Dashboard/Components/Panels.vue'

const SweetAlert = () =>
  import ('src/pages/Dashboard/Components/SweetAlert.vue')
import Notifications from 'src/pages/Dashboard/Components/Notifications.vue'
import Icons from 'src/pages/Dashboard/Components/Icons.vue'
import Typography from 'src/pages/Dashboard/Components/Typography.vue'
import WelcomePage from "../CANDIDAT/pages/Dashboard/WelcomePage/WelcomePage";



// Forms pages
const RegularForms = () =>
  import ('src/pages/Dashboard/Forms/RegularForms.vue')
const ExtendedForms = () =>
  import ('src/pages/Dashboard/Forms/ExtendedForms.vue')
const ValidationForms = () =>
  import ('src/pages/Dashboard/Forms/ValidationForms.vue')
const Wizard = () =>
  import ('src/pages/Dashboard/Forms/Wizard.vue')
const WelcomCompany = () =>
  import ('src/Company/pages/Dashboard/Company/Welcom.vue')
// Company
const RegisterCompany = () =>
  import ('src/BO/pages/Dashboard/Company/Company.vue')
const CompanyList = () =>
  import ('src/BO/pages/Dashboard/Company/CompanyList.vue')

// Candidate
const RegisterCandidate = () =>
  import ('src/Company/pages/Dashboard/Candidate/Candidate.vue')


const CandidateList = () =>
  import ('src/Company/pages/Dashboard/Candidate/CandidateList.vue')
// Prms
const PrmCategoryRoute = () =>
  import ('src/BO/pages/Dashboard/PrmCategory/PrmCategory.vue')

const PrmLevelRoute = () =>
  import ('src/BO/pages/Dashboard/PrmLevel/PrmLevel.vue')

const PrmTechnologyRoute = () =>
  import ('src/BO/pages/Dashboard/PrmTechnology/PrmTechnology.vue')

const PrmDifficultyRoute = () =>
  import ('src/BO/pages/Dashboard/PrmDifficulty/PrmDifficulty.vue')

const PrmAccessRoute = () =>
  import ('src/BO/pages/Dashboard/PrmAccess/PrmAccess.vue')

// Deals
const DealRoute = () =>
  import ('src/BO/pages/Dashboard/Deals/Deals.vue')

// Question And Rsponses
const QuestionList = () =>
  import ('src/EXAMINATEUR/pages/Dashboard/QuestionResponse/QuestionList.vue')


const QuestionResponse = () =>
  import ('src/EXAMINATEUR/pages/Dashboard/QuestionResponse/QuestionResponse.vue')

const QuestionResponseEdit = () =>
  import ('src/EXAMINATEUR/pages/Dashboard/QuestionResponse/QuestionResponseEdit.vue')

// TableList pages
const RegularTables = () =>
  import ('src/pages/Dashboard/Tables/RegularTables.vue')
const ExtendedTables = () =>
  import ('src/pages/Dashboard/Tables/ExtendedTables.vue')
const PaginatedTables = () =>
  import ('src/pages/Dashboard/Tables/PaginatedTables.vue')
// Maps pages
const GoogleMaps = () =>
  import ('src/pages/Dashboard/Maps/GoogleMaps.vue')
const FullScreenMap = () =>
  import ('src/pages/Dashboard/Maps/FullScreenMap.vue')
const VectorMaps = () =>
  import ('src/pages/Dashboard/Maps/VectorMapsPage.vue')

// Calendar
const Calendar = () =>
  import ('src/pages/Dashboard/Calendar/CalendarRoute.vue')
// Charts
const Charts = () =>
  import ('src/pages/Dashboard/Charts.vue')
// Displaying  Test Questions
const DisplayingQuestions = () =>
  import ('src/Company/pages/Dashboard/TestAccess/DisplayingQuestions.vue')

//Validating Test
const ValidatingTest = () =>
  import ('src/Company/pages/Dashboard/TestAccess/TestValidation.vue')
// testAccess
const TestAccess = () =>
  import ('src/Company/pages/Dashboard/TestAccess/TestAccess.vue')

// testCreate
const TestCreate = () =>
  import ('src/Company/pages/Dashboard/TestAccess/TestCreate.vue')

const TestDoneList = () =>
  import ('src/Company/pages/Dashboard/TestCreate/TestDoneList.vue')

const TestCreateStep1 = () =>
  import ('src/Company/pages/Dashboard/TestAccess/TestCreateStep1.vue')
const AllTests = () =>
  import ('src/Company/pages/Dashboard/TestCreate/AllTests.vue')

//Package
const Package = () =>
  import ('src/Company/pages/Dashboard/Package/Package.vue')

//Candidate
const WelcomeCandidate = () =>
  import ('src/CANDIDAT/pages/Dashboard/WelcomePage/WelcomePage.vue')
const FinishCandidate= () =>
  import ('src/CANDIDAT/pages/Dashboard/TestFinished/TestFinished.vue')
const PassageCandidat = () =>
  import ('src/CANDIDAT/pages/Dashboard/Passage/Passage.vue')
const TopCandidates = () =>
  import ('src/Company/pages/Dashboard/Stats/TopCandidates.vue')
const PassageDuration = () =>
  import ('src/Company/pages/Dashboard/Stats/PassageDurationStats.vue')
const TestDone = () =>
  import ('src/Company/pages/Dashboard/Stats/TestDone.vue')
const PercentagePerTechno = () =>
  import ('src/Company/pages/Dashboard/Stats/PercentagePerTechnology.vue')
const PercentagePerScore = () =>
  import ('src/Company/pages/Dashboard/Stats/PercentagePerScore.vue')
const TestDetails = () =>
  import ('src/Company/pages/Dashboard/TestCreate/TestDetails.vue')
const PackageDetails = () =>
  import ('src/Company/pages/Dashboard/TestCreate/PackageDetails.vue')
const PackageStatistics = () =>
  import ('src/Company/pages/Dashboard/TestCreate/Statistics.vue')

let componentsMenu = {
  path: '/components',
  component: DashboardLayout,
  redirect: '/components/buttons',
  children: [{
    path: 'buttons',
    name: 'Buttons',
    component: Buttons
  },
    {
      path: 'grid-system',
      name: 'Grid System',
      component: GridSystem
    },
    {
      path: 'panels',
      name: 'Panels',
      component: Panels
    },
    {
      path: 'sweet-alert',
      name: 'Sweet Alert',
      component: SweetAlert
    },
    {
      path: 'notifications',
      name: 'Notifications',
      component: Notifications
    },
    {
      path: 'icons',
      name: 'Icons',
      component: Icons
    },
    {
      path: 'typography',
      name: 'Typography',
      component: Typography
    }

  ]
}
let formsMenu = {
  path: '/forms',
  component: DashboardLayout,
  redirect: '/forms/regular',
  children: [{
    path: 'regular',
    name: 'Regular Forms',
    component: RegularForms
  },
    {
      path: 'extended',
      name: 'Extended Forms',
      component: ExtendedForms
    },
    {
      path: 'validation',
      name: 'Validation Forms',
      component: ValidationForms
    },
    {
      path: 'wizard',
      name: 'Wizard',
      component: Wizard
    }
  ]
}
let WelcomComp={
  path: '/evaluation/company/welcom',
    name:'Welcom',
    component: WelcomCompany
}
let  Company={
  path:'/bo',
  component:DashboardBo,
  children:[{
    path:'company/add',
    name:'Company',
    component:RegisterCompany
  },
    {
      path:'company',
      name:'CompanyList',
      component:CompanyList
    }]
}
  let Statistics = {
  path: '/company',
  component: DashboardCompany,
  children: [{
    path: 'stats/topCandidates',
    name: 'TopCandidates',
    component: TopCandidates
  }, {
    path: 'stats/passageDuration',
    name: 'Passage Duration',
    component: PassageDuration
  },
    {
      path: 'stats/TestDone',
      name: 'Test Done',
      component: TestDone
    },
    {
      path: 'stats/PercentagePerTechno',
      name: 'Percentage Per Technology',
      component: PercentagePerTechno
    },

    {
      path: 'stats/PercentagePerScore',
      name: 'Percentage Per Score',
      component: PercentagePerScore
    },

  ]}

let Prms = {
  path: '/bo',
  component: DashboardBo,
  children: [{
    path: 'prmCategory',
    name: 'Categories',
    component: PrmCategoryRoute
  },
    {
      path: 'prmLevel',
      name: 'Levels',
      component: PrmLevelRoute
    },
    {
      path: 'prmTechnology',
      name: 'Technologies',
      component: PrmTechnologyRoute
    },
    {
      path: 'prmDifficulty',
      name: 'Difficulties',
      component: PrmDifficultyRoute
    },
    {
      path: 'prmAccess',
      name: 'Access',
      component: PrmAccessRoute
    }
  ]
}

let Deals = {
  path: '/bo',
  component: DashboardBo,
  children: [{
    path: 'deal',
    name: 'Deals',
    component: DealRoute
  }
  ]
}
let FinishCandidat={

  path: '/candidat/finish/:id',
  name: 'finish candidat',
  component: FinishCandidate
}
let WelcomCandidat={

  path: '/candidat/welcome/:id',
  name: 'welcome candidat',
  component: WelcomeCandidate
}

let Candidate = {
  path: '/candidat/passage/:id',
  name: 'passage candidat',
  component: PassageCandidat
}

let QuestionResponses = {
  path: '/examinateur',
  component: DashboardExaminateur,
  children: [{
    path: 'questionResponses',
    name: 'Questions And Responses',
    component: QuestionResponse
  }, {
    path: 'questionResponses/edit',
    name: 'Edit Questions And Responses',
    component: QuestionResponseEdit
  },
    {
      path: 'questionList',
      name: 'Questions',
      component: QuestionList
    }

  ]
}



let tablesMenu = {
  path: '/table-list',
  component: DashboardLayout,
  redirect: '/table-list/regular',
  children: [{
    path: 'regular',
    name: 'Regular Tables',
    component: RegularTables
  },
    {
      path: 'extended',
      name: 'Extended Tables',
      component: ExtendedTables
    },
    {
      path: 'paginated',
      name: 'Paginated Tables',
      component: PaginatedTables
    }
  ]
}

let mapsMenu = {
  path: '/maps',
  component: DashboardLayout,
  redirect: '/maps/google',
  children: [{
    path: 'google',
    name: 'Google Maps',
    component: GoogleMaps
  },
    {
      path: 'full-screen',
      name: 'Full Screen Map',
      component: FullScreenMap
    },
    {
      path: 'vector-map',
      name: 'Vector Map',
      component: VectorMaps
    }
  ]
}

let pagesMenu = {
  path: '/pages',
  component: DashboardLayout,
  redirect: '/pages/user',
  children: [{
    path: 'user',
    name: 'User Page',
    component: User
  },
    {
      path: 'timeline',
      name: 'Timeline Page',
      component: TimeLine
    }
  ]
}

let loginPage = {
  path: '/evaluation/login',
  name: 'Login',
  component: Login
}

let registerPage = {
  path: '/evaluation/register',
  name: 'Register',
  component: Register
}

let lockPage = {
  path: '/lock',
  name: 'Lock',
  component: Lock
}

let logoutPage = {
  path: '/logout',
  name: 'Logout',
  component: Logout
}

let CompanyMenu =  {
  path: '/company',
  component: DashboardCompany,
  redirect: '/company/test/package',
  children: [{
    path:'candidate/add',
    name:'Candidate',
    component:RegisterCandidate
  },
    {
      path:'candidate',
      name:'CandidateList',
      component:CandidateList
    },
    {
    path: 'test/access',
    name: 'Test Access',
    component: TestAccess
  },
    {
      path: 'test/display/:packid',
      name: 'Choose Questions',
      component: DisplayingQuestions
    },
    {
      path: 'test/validate/:packid',
      name: 'Validate Test',
      component: ValidatingTest
    }
    , {
      path: 'test/create/:packid',
      name: 'Create Test',
      component: TestCreate
    },
    {
      path: 'test/done/:packid',
      name: 'Test Done',
      component: TestDoneList
    },
    {
      path: 'test/details/:testid',
      name: 'Test Details',
      component: TestDetails
    },

    {
      path: 'test/create/step1/:packid',
      name: 'Create Test',
      component: TestCreateStep1
    }

    ,

    {
      path: 'allTests',
      name: 'All Tests',
      component: AllTests
    },{
      path: 'test/package',
      name: 'Package',
      component: Package
    },
    ,{
      path: 'test/package/statistics/:packid',
      name: 'Statistics',
      component: PackageStatistics
    },
    {
      path: 'test/packageDetails/:packid',
      name: 'Package Details',
      component: PackageDetails
    }
  ] }

const routes = [{
  path: '/',
  redirect: '/evaluation/login'
},
  componentsMenu,
  formsMenu,
  tablesMenu,
  Company,
  Prms,
  Statistics,
 Deals,
  QuestionResponses,
  CompanyMenu,
  mapsMenu,
  pagesMenu,
  Candidate,
  WelcomCandidat,
  FinishCandidat,
 // Companyy,
  loginPage,
  WelcomComp,
  registerPage,
  lockPage,
  logoutPage,
  {
    path: '/admin',
    component: DashboardLayout,
    redirect: '/admin/overview',
    children: [{
      path: 'overview',
      name: 'Overview',
      component: Overview
    },
      {
        path: 'stats',
        name: 'Stats',
        component: Stats
      },
      {
        path: 'calendar',
        name: 'Calendar',
        component: Calendar
      },
      {
        path: 'charts',
        name: 'Charts',
        component: Charts
      }
    ]
  },
  {
    path: '/bo',
    component: DashboardBo,
    redirect: '/bo/overview',
    children: [{
      path: 'overview',
      name: 'OverviewBo',
      component: OverviewBo
    },
    ]
  },
  {path: '*', component: NotFound}
]

export default routes
